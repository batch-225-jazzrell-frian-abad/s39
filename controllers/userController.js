const User = require('../models/user');
const bcrypt = require('bcrypt');
const auth = require('../auth');




module.exports.checkEmailExist = (reqBody) => {

	return User.find({email : reqBody.email}).then(result => {

		// the findOne method returns a record if a match is found

		if (result.length > 1) {

			return {
				message : 'Duplicate User'
			}

		} else if (result.length > 0) {

			return {
				message: 'User was found'
			}

		} else {

			return {
				message: 'User not found'
			}
		}

	})
}



module.exports.registerUser = (reqBody) => {


	let newUser = new User({

		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)
	}) 

	return newUser.save().then((user, error) => {

		if (error) {

			return false;

		} else {

			return user

		};
	});
};


// User Authentication

/*
	Steps:
	1. Check the database if the user email exists 
	2. Compare the password provided in the login form with the password stored in the database
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not.
*/

// ACTIVITY s39
module.exports.loginUser = (reqBody) => {

	return User.findOne({email : reqBody.email}).then(result => {

		if (result == null) {

			return {
				message: "Not Found in our Database"
			}

		} else {

			// The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password); 

			if (isPasswordCorrect) {

				return {access: auth.createAccessToken(result)}

			} else {

				// if password doesn't match
				return {
					message : "Password was Incorrect"
				}
			};
		};

	});
};








// ===================================================

// Activity s38

/*module.exports.getProfile = (userID, newContent) => {

	return User.findById(userID).then((result, err) => {

		if (err){
			console.log(err)
			return false;
		} 
		else {
			
			result.password = "  ";

			return result.save().then((updated, saveErr) => {
				if (saveErr) {
					console.log(saveErr);
					return false;
				} else{
					return updated;
				}
			})
		}
	})
}*/
// ACTIVITY ANSWER s38
/*module.exports.getProfile = (userID, newContent) => {

	return User.findById(userID).then(result => {

			result.password = "";

			return result;
		
	})
}*/



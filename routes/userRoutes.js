const express = require('express');
const router = express.Router();

const userController = require('../controllers/userController')

// Check Email 
router.post('/checkEmail', (req, res) => {
	userController.checkEmailExist(req.body).then(result => res.send(result));
});

// Route for registration

router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result));

});

// Activity s38

router.post('/details', (req, res) => {
	userController.getProfile(req.body).then(result => res.send(result));
});

// // ACTIVITY s39
router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result));
})






module.exports = router;